#include "servo.hpp"

ArduinoBuffer<64> buffer(&Serial);

emb::device::EmbMessenger<2, 0> messenger(&buffer, millis);

Servo sv;

void ServoReadAdaptor()
{
    uint16_t angle = 0;
    angle = sv.read();
    messenger.write(angle);
}

void ServoWriteAdaptor()
{
    uint8_t address = 0;
    uint16_t angle = 0;
    messenger.read(angle);
    sv.write(angle);
    messenger.write(angle);
}

void setup()
{
    sv.attach();
    Serial.begin(9600);
    messenger.registerCommand(0, ServoReadAdaptor);
    messenger.registerCommand(1, ServoWriteAdaptor);
}