#include <rip/components/embed_commands.hpp>

namespace commands 
{
    ServoRead::ServoRead(int16_t angle)
        : m_angle(angle)
    {}

    void ServoRead::send(EmbMessenger* messenger)
    {
        messenger->write(m_angle);
    }
    
    bool ServoRead::getResult() const 
    {
        return m_angle;
    }

    ServoWrite::ServoWrite(int16_t angle)
        : m_angle(angle)
    {}

    void ServoWrite::send(EmbMessenger* messenger)
    {
        messenger->write(m_angle);
    }

}
