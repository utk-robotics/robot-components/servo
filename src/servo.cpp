#include "rip/components/servo.hpp"
#include "rip/components/embed_commands.hpp"

namespace rip::components
{

    Servo::Servo(const std::string& name, const nlohmann::json& config, 
            std::shared_ptr<emb::host::EmbMessenger> emb, 
            std::shared_ptr<std::unordered_map<std::string, 
                std::shared_ptr<RobotComponent> > > comps)
    : RobotComponent(name, config, emb, comps)
    {}

    std::vector<std::string> Servo::diagnosticCommands() const
    {
        std::vector< std::string > s;
        return s;
    }

    void Servo::registerCommands()
    {
        m_messenger->registerCommand<commands::ServoRead>(commandIds->at("kServoRead"));
        m_messenger->registerCommand<commands::ServoWrite>(commandIds->at("kServoWrite"));
    }

    void Servo::ServoWrite(uint16_t angle)
    {
        auto cmd = std::make_shared<commands::
        m_messenger->send(angle);
    }

    int Servo::ServoRead()
    {
        uint16_t angle;
        m_messenger->read<uint16_t>(angle);
        return angle;
    }

    void Servo::stop()
    {

    }

}
