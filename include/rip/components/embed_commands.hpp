#ifndef EMBED_COMMANDS_HPP
#define EMBED_COMMANDS_HPP
// #include <fmt/format.h>
// #include <rip/misc/logger.hpp>
#include <rip/components/servo.hpp>
#include <tuple>
#include <stdint.h>
#include <EmbMessenger/Command.hpp>
// #include <json.hpp>
// #include <units/units.hpp>
#include <EmbMessenger/EmbMessenger.hpp>

#define SERVO_COMMANDS_HPP
using emb::host::Command;
using emb::host::EmbMessenger;

namespace commands {
    class ServoRead : public Command
    {
        uint16_t m_angle;
    public:
        ServoRead();
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        bool getResult() const; 
    };

    class ServoWrite : public Command
    {
        uint16_t m_angle;
    public:
        ServoWrite(uint16_t angle);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
    };
}
#endif
