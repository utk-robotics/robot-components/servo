#ifndef SERVO_HPP
#define SERVO_HPP

#include <rip/robot_component.hpp>

namespace rip::components 
{

    class Servo : public RobotComponent 
    {
        public:
            Servo(const std::string& name, const nlohmann::json& config,
                    std::shared_ptr<emb::host::EmbMessenger> emb,
                    std::shared_ptr<std::unordered_map<std::string, 
                        std::shared_ptr<RobotComponent> > > comps);

            virtual std::vector<std::string> diagnosticCommands() const override;

            virtual void registerCommands();
            
            virtual void ServoWrite(uint16_t angle);

            virtual int ServoRead();

            virtual void stop() override;
    };

}

#endif // SERVO_HPP
